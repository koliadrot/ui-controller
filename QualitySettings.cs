﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.Dropdown;

public class QualitySettings : MonoBehaviour
{
    private enum ClippingPlane
    {
        Low,
        Medium,
        High
    }
    public Camera cam;
    public Dropdown dropdown;
    private ClippingPlane clippingPlane;

    private void Start()
    {
        dropdown.ClearOptions();
        for (int i = 0; i < 3; i++)
        {
            clippingPlane = (ClippingPlane)i;
            OptionData optionData = new OptionData();
            optionData.text = Enum.GetName(typeof(ClippingPlane), clippingPlane).ToString();
            dropdown.options.Add(optionData);
        }
        dropdown.onValueChanged.AddListener(ctx => OnChangeLanguage());
        dropdown.value = PlayerPrefs.GetInt("QualitySetting") == 0 ? -1 : PlayerPrefs.GetInt("QualitySetting");
    }

    private void OnDestroy()
    {
        dropdown.onValueChanged.RemoveListener(ctx => OnChangeLanguage());
    }
    private void Lod(ClippingPlane clippingPlane)
    {
        float baseValue = 2;
        cam.farClipPlane = ((int)clippingPlane+baseValue) * 20
            ;
    }

    private void OnChangeLanguage()
    {
        clippingPlane = (ClippingPlane)dropdown.value;
        Lod(clippingPlane);
    }
}
