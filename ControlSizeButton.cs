﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ControlSizeButton : MonoBehaviour
{
    public string field;
    private RectTransform select;
    public float defaultScreenWidth = 1280f;
    public float defaultScreenHeight = 720f;
    private float currentSizeX;
    private float currentSizeY;
    private Vector2 currentPos;
    private Vector3 newPos;
    private float ratio;
    private char[] separators = new char[] { ' ', ',', '%', '/' };

    private void Awake()
    {
        select = GetComponent<RectTransform>();
        float defaultXRatio = Screen.width / defaultScreenWidth;//screen
        float defaultYRatio = Screen.height / defaultScreenHeight;//screen
        select.sizeDelta = new Vector2(select.sizeDelta.x * defaultXRatio, select.sizeDelta.y * defaultYRatio);//screen
        select.anchoredPosition = new Vector2(select.anchoredPosition.x * defaultXRatio, select.anchoredPosition.y * defaultYRatio);
        currentSizeX = select.sizeDelta.x;
        currentSizeY = select.sizeDelta.y;
        ChangeSize();
    }
    public void ChangeSize()
    {
        try
        {
            ratio = float.Parse(field.Split(separators, System.StringSplitOptions.RemoveEmptyEntries)[0]) / 100f;
        }
        catch
        {
            ratio = PlayerPrefs.GetFloat(select.name + " Ratio", 1);
        }
        select.sizeDelta = new Vector2(currentSizeX * ratio, currentSizeY * ratio);
        PlayerPrefs.SetFloat(select.name + " Ratio", ratio);
        Spacing();
    }
    public void Spacing()
    {
        currentPos = RectTransformUtility.WorldToScreenPoint(new Camera(), transform.position);
        currentPos.x = Mathf.Clamp(currentPos.x, select.rect.size.x / 2f, Screen.width - select.rect.size.x / 2f);
        currentPos.y = Mathf.Clamp(currentPos.y, select.rect.size.y / 2f, Screen.height - select.rect.size.y / 2f);
        RectTransformUtility.ScreenPointToWorldPointInRectangle(select, currentPos, new Camera(), out newPos);
        transform.position = newPos;
    }
}
