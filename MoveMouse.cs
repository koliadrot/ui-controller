﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class MoveMouse : MonoBehaviour
{

    /// <summary>
    /// Скрипт управления камерой через мышку
    /// GameObject - объект, к которому привязана камера (вращаемся вокруг этого объекта)
    /// sensitivity - чувствительность мышки
    /// </summary>

    public float sensitivity = 1F;
    private Camera goCamera;
    private Vector3 MousePos;
    private float MyAngle = 0F;

    void Start()
    {
        // Инициализируем кординаты мышки и ищем главную камеру
        goCamera = FindObjectOfType<Camera>();
        Cursor.visible = false;
    }

    //void Update()
    //{
    //    MousePos = Input.mousePosition;
    //}

    //void FixedUpdate()
    //{
    //    MyAngle = 0;
    //    // расчитываем угол, как:
    //    // разница между позицией мышки и центром экрана, делённая на размер экрана
    //    //  (чем дальше от центра экрана тем сильнее поворот)
    //    // и умножаем угол на чуствительность из параметров
    //    MyAngle = sensitivity * ((MousePos.x - (Screen.width / 2)) / Screen.width);
    //    goCamera.transform.RotateAround(go.transform.position, goCamera.transform.up, MyAngle);
    //    MyAngle = sensitivity * ((MousePos.y - (Screen.height / 2)) / Screen.height);
    //    goCamera.transform.RotateAround(go.transform.position, goCamera.transform.right, -MyAngle);
    //    Debug.Log(Mathf.Clamp(goCamera.transform.eulerAngles.x, -90f, 90f));
    //    goCamera.transform.eulerAngles = new Vector3(goCamera.transform.eulerAngles.x, goCamera.transform.eulerAngles.y, 0f);
    //}

    private void FixedUpdate()
    {

        // Mouse Abort
        float MouseX = Input.GetAxis("Mouse X");
        float MouseY = Input.GetAxis("Mouse Y");
        goCamera.transform.rotation = goCamera.transform.rotation * Quaternion.AngleAxis(MouseX, Vector3.up);
        goCamera.transform.rotation = Quaternion.AngleAxis(-MouseY, Vector3.right) * goCamera.transform.rotation;

        goCamera.transform.eulerAngles = new Vector3(ClampEulerAngles(goCamera.transform.eulerAngles.x, -90f, 90f), ClampEulerAngles(goCamera.transform.eulerAngles.y,-90f,90f), 0f);
    }

    public float ClampEulerAngles(float val, float min, float max)
    {
        val = GetEulerAngles(val);
        if (val.CompareTo(min) < 0) return min;
        else if (val.CompareTo(max) > 0) return max;
        else return val;
    }
    private float GetEulerAngles(float value)
    {
        if (value > 180f) return value-360;
        return value;
    }

}